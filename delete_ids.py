from pathlib import Path
import glob
import aiosqlite as lite
import asyncio

# delete.txt is file with list of wg ids from wg request for deletion
# id,id,id
# theoretically, if request is more then 30 days old, there is nothing to delete as we delete older id automatically anyways

with open("delete.txt") as d:
    n = d.read().split(",")


async def db():
    async with lite.connect(
        "data/users.db", detect_types=lite.core.sqlite3.PARSE_DECLTYPES
    ) as db:
        for i in n:
            # print(i)
            async with db.execute(
                "select * from users where account_id=?", (i,)
            ) as cursor:
                data = await cursor.fetchone()
                if data:
                    print(i, data)
                    await db.execute("delete from users where account_id=?", (i,))
                    await db.commit()


async def saved():
    for i in n:
        # for p in Path("stats/daily/").glob("*{}*".format(i)):
        for p in glob.glob("stats/**/*{}*".format(i), recursive=True):
            Path(p).unlink()
            print(i, p)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    # loop.run_until_complete(db())
    loop.run_until_complete(saved())
