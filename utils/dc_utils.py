import discord


class DC:
    def __init__(self, bot):
        self.bot = bot

    async def typing(self, ctx):
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        try:
            await ctx.trigger_typing()
        except discord.Forbidden:
            self.bot.logger.info("Cannot send any message to channel, contacting user")
            await ctx.message.author.send(
                content=_(
                    "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                )
            )
            if ctx.message.guild is not None:
                self.bot.logger.warning(
                    (ctx.message.guild.name, ctx.message.author.name)
                )
            else:
                self.bot.logger.warning((ctx.message.author.name))

    async def not_found_msg(self, ctx, out):
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        try:
            await ctx.message.add_reaction("\U0001f4e8")
        except discord.Forbidden:
            pass
        await ctx.message.author.send(content=out)
