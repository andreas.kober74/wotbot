#extract string
pybabel extract -o messages.pot .

#new language, run only once!
pybabel init -i messages.pot -d translations -l cz_CZ

#after string (re) extraction, update strings
pybabel update -i messages.pot -d translations

#after traslation, upload ne po files and recompile
pybabel compile -d translations
