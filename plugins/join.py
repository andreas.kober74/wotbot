import discord
from discord.ext import commands
from discord import colour
import asyncio
from operator import itemgetter
import random

import json
from tabulate import tabulate
from collections import Counter
from operator import itemgetter
from collections import OrderedDict
import pickle
import datetime
from pathlib import Path
import humanize
import aiohttp
import math
import gzip
import os
import zlib


class Utilities:
    def __init__(self, bot):
        self.bot = bot

    async def on_member_join(self, member):
        """Welcome member"""
        self.bot.logger.info("welcome member {0.name} {0.guild.name} ".format(member))

        player_name = await self.bot.wg.get_local_name_by_id(member.id)
        prefix = await self.bot.get_prefix(member)

        if player_name is not None:
            return

        _ = self.bot.lang[self.bot.set_lang_member(member)].gettext

        embed = discord.Embed(
            title=_("Welcome from WotBot!"),
            description=_(
                "Hello {member.name}, welcome to {member.guild.name}, i am WotBot, your statistics bot for WoT Blitz!\n\nTo recognize you as a WoT Blitz player, please continue with the following link to the [WotBot Dashboard](https://wotbot.pythonanywhere.com/) to set your WoT Blitz name and to adjust your WotBot preferences (for example language). Sign now to [WotBot Dashboard](https://wotbot.pythonanywhere.com/) with Discord, go to Settings, Continue, then use the 'Link your WG account' button to log in with your Wargaming account. You can also check the [help](https://wotbot.pythonanywhere.com/help) there, but if you ever don't know what to do just tag me or send me a message with `{prefix}help` or `@WotBot help` for help."
            ).format(member=member, prefix=prefix[0]),
            colour=234,
            type="rich",
        )
        embed.set_author(
            name="WotBot",
            url="https://wotbot.pythonanywhere.com/",
            icon_url="https://wotbot.pythonanywhere.com/static/images/fav/favicon-114.png",
        )

        embed.set_footer(text=_("Have a good day, your \U0001f451 WotBot"))
        try:
            await member.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.info("Cannot contact user, forbidden")


def setup(bot):
    bot.add_cog(Utilities(bot))
