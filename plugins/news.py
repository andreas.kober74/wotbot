import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
from tabulate import tabulate
import datetime
import re


class News:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=True)
    # @asyncio.coroutine
    async def news_servers(self, ctx):
        """list friendly servers"""
        print("list servers")
        for server_id in self.bot.ServerCfg:
            if "updates-channel" in self.bot.ServerCfg[server_id]:
                room_id = self.bot.ServerCfg[server_id]["updates-channel"]
                server = self.bot.get_guild(int(server_id))
                try:
                    self.bot.logger.info(("Server: ", server.name))
                except:
                    self.bot.logger.error(("Server name doesn't exist: ", server_id))

    @commands.command(pass_context=True, hidden=True)
    # @asyncio.coroutine
    async def send_news(self, ctx):
        """Send a update news to friendly servers"""

        embed = discord.Embed(
            title="WotBot news",
            colour=discord.Colour(0x453BD3),
            url="https://wotbot.pythonanywhere.com/",
            description="Hello, this is your **WotBot** here. Bringing you some news from the WotBot development hangar.",
        )
        embed.set_author(
            name="b48g55m",
            url="https://wotbot.pythonanywhere.com/",
            icon_url="https://cdn.discordapp.com/avatars/345299188986150923/8f6ea60cfb9004b097623907a19f0bbf.png?size=256",
        )
        embed.set_image(
            url="https://user-images.githubusercontent.com/3680926/45116362-beb88b00-b152-11e8-948a-bd5ab7cb5145.png"
        )
        embed.set_footer(
            text="You are receiving this message because you have been subscribed to WotBot News channel. News are sent infrequently :)",
            icon_url="https://user-images.githubusercontent.com/3680926/45113636-2e2a7c80-b14b-11e8-8cb3-05d7bff11370.png",
        )
        embed.add_field(
            name="Rating battles",
            value="Normally, all statistics tools concentrate on Random battles only. Uniquely, the daily command in WotBot has been able to calculate statistics for Rating battles for a while now, which allows you to watch your stats more closely so you can try to improve where needed. Hope you'll find it useful.",
        )

        embed.add_field(
            name="WotBot Dashboard and WotB Widget",
            value="[WotBot Dashboard](https://wotbot.pythonanywhere.com/) has become a popular tool to set your personal and server WotBot preferences. I am happy to see that this development effort has been useful. [WotB Widget](https://widget.pythonanywhere.com/) is much more specialized tool, used mainly by people who do live streaming on YouTube, Twitch or other places. It can calculate and display your current live statistics, like winrate, damage per battle or number of battles during your stream. It has been translated to multiple languages and can now also track your Rating battles.",
        )

        embed.add_field(
            name="Looking for translators",
            value="Thanks to multiple people (big kudos to all existing helpers!), the web interfaces (WotBot Dashboard and WotB Widget) and also several WotBot commands (Daily, Config, BlitzQuiz, Replays) are now available in multiple languages. I am looking for more helpers to allow other non English speakers to use and more understand the WotBot commands and thus to learn how to read the statistics and be better players. If you like to help, please come to push the translating effort - simply log to [Crowdin](https://crwd.in/wotbot) (online translation tool) and get translating right away. Or you can also just read-proof, correct and confirm translations... any help is welcome! If your language is not listed as available for translation, just ping me, i will add it, see contact below.",
        )

        embed.add_field(
            name="WotBot",
            value="Not only has WotBot been added by more then 1500 servers, but the usage of commands has also been going up. For a small author like me it is great to have a tool that people like. If you have an idea of what is missing, what could be made better or if you like to help with some coding in Python, stop by at the [WotBot Labs discord server](https://discordapp.com/invite/mzXYPVW).",
        )

        embed.add_field(name="\uFEFF", value="Enjoy your tanking, **_b48g55m_**")
        # this supports [named links](https://discordapp.com) on top of the previously shown subset of markdown.")
        # embed = discord.Embed(title="title ~~(did you know you can have markdown here too?)~~", colour=discord.Colour(0x453bd3), url="https://discordapp.com", description="this supports [named links](https://discordapp.com) on top of the previously shown subset of markdown. ```\nyes, even code blocks```", timestamp=datetime.datetime.utcfromtimestamp(1511804660))
        # embed.set_image(url="https://cdn.discordapp.com/embed/avatars/0.png")
        # embed.set_thumbnail(url="https://cdn.discordapp.com/embed/avatars/0.png")
        # embed.add_field(name="a", value="try exceeding some of them!")
        # embed.add_field(name="a", value="an informative error should show up, and this view will remain as-is until all issues are fixed")
        # embed.add_field(name="<:thonkang:219069250692841473>", value="these last two", inline=True)
        # embed.add_field(name="<:thonkang:219069250692841473>", value="are inline fields", inline=True)
        # await ctx.send(content="this `supports` __a__ **subset** *of* ~~markdown~~  ```js\nfunction foo(bar) {\n  console.log(bar);\n}\n\nfoo(1);```", embed=embed)
        await ctx.send(content=None, embed=embed)

        count_ok = 0
        count_nok = 0
        for server_id in self.bot.ServerCfg:
            if "updates-channel" in self.bot.ServerCfg[server_id]:
                room_id = self.bot.ServerCfg[server_id]["updates-channel"]
                server = self.bot.get_guild(int(server_id))
                try:
                    self.bot.logger.info(("sending to: ", server.name))
                except Exception as e:
                    self.bot.logger.info(e)
                    self.bot.logger.error(("Server doesn't exist: ", server))

                try:
                    await self.bot.get_channel(int(room_id)).send(
                        content=None, embed=embed
                    )
                    count_ok += 1
                except Exception as e:
                    self.bot.logger.info(e)
                    self.bot.logger.error("sending failed")
                    count_nok += 1
        print("Done. OK: {}, Not OK: {}".format(count_ok, count_nok))


def setup(bot):
    bot.add_cog(News(bot))
