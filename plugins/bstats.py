import discord
from discord.ext import commands

# import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
import aiohttp


class BlitzStarsStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False, aliases=["btstats", "bstat"])
    # @asyncio.coroutine
    async def bstats(self, ctx, *, player_name: str = None):
        """BlitzStars top stats."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        if player_name is None or ctx.message.mentions:
            player_name = await self.bot.wg.get_local_name(ctx)
        self.bot.logger.info(("bsig:", player_name))

        player, region = await self.bot.wg.get_player_a(player_name, ctx.message)
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]
            # import async_timeout
            # with async_timeout.timeout(10):
            async with aiohttp.ClientSession(loop=self.bot.loop) as client:
                url = "https://www.blitzstars.com/api/top/player/{}".format(player_id)
                async with client.get(url) as r:
                    data = None
                    if r.status == 200:
                        data = await r.json(content_type=None)
                        if data:
                            # data=json.loads(r.text)
                            self.bot.logger.debug(data)

                    if data is not None:
                        periods = ["period30d", "period60d", "period90d", "statistics"]
                        toutotal = _("Data is always about 24h old.\n\n")
                        for period in periods:
                            toutotal += "{}:\n".format(
                                period.replace("statistics", _("lifetime"))
                                .replace("period30d", _("period30d"))
                                .replace("period60d", _("period60d"))
                                .replace("period90d", _("period90d"))
                            )
                            tout = []
                            if (data[period]) == None:
                                await ctx.send(
                                    _("No data for `{}@{}` ").format(
                                        player_nick, region
                                    )
                                )
                                return
                            if not data[period].get("all", False):
                                await ctx.send(_("No data on BlitzStars"))
                                return
                            if "tierData" in data[period]:
                                tdata = data[period]["tierData"]["tiers"]
                                tout = []
                                for t in tdata:
                                    tout.append(
                                        [
                                            tdata[t]["tier"],
                                            tdata[t]["battles"],
                                            tdata[t]["wins"]
                                            / tdata[t]["battles"]
                                            * 100,
                                        ]
                                    )

                            head = [
                                [_("Battles"), data[period]["all"]["battles"]],
                                [_("Winrate"), data[period]["special"]["winrate"]],
                            ]
                            toutotal += "{}\n\n".format(
                                tabulate(head, floatfmt=".2f", numalign="right")
                            )
                            if tout:
                                tout_sorted = sorted(
                                    tout, key=itemgetter(0, 1), reverse=False
                                )
                                toutotal += "\n{}\n\n".format(
                                    tabulate(
                                        tout_sorted,
                                        [_("Tier"), _("Battles"), _("WR")],
                                        floatfmt=".2f",
                                        numalign="right",
                                    )
                                )
                    else:
                        toutotal = _("No data")
            self.bot.logger.debug(len(str(toutotal)))
            embed = discord.Embed(
                title=_("BlitzStars statistics for {}@{}").format(player_nick, region),
                description="```{}```".format(toutotal),
                colour=234,
                type="rich",
                url="https://www.blitzstars.com/player/{}/{}".format(
                    region, player_nick
                ),
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
            embed.set_thumbnail(
                url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png"
            )
            embed.set_footer(
                text="Statistics by BlitzStars",
                icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png",
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(BlitzStarsStats(bot))
