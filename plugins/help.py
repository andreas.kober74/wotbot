import discord
from discord.ext import commands
from discord.ext.commands import bot as bot_module
import asyncio
import inspect


class Utilities:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        name="help",
        aliases=["wotbot", "hello", "command", "commands"],
    )
    # @asyncio.coroutine
    async def aryas_help(self, ctx, *commands_):
        """Shows help message."""
        # Adapted from discord.ext.commands.bot._default_help_command.
        # The only thing that has been changed is the last send command
        # which now sends embeds instead of normal text messages
        self.bot.logger.info("help")

        # get ready for translations. must fogure out command __doc__ switching for help...
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext

        destination = ctx.message.author
        # if self.bot.pm_help else ctx.message.channel
        destination_quick_help = (
            ctx.message.author if self.bot.pm_help else ctx.message.channel
        )

        def repl(obj):
            return bot_module._mentions_transforms.get(obj.group(0), "")

        # help by itself just lists our own commands.
        if len(commands_) == 0:
            # fast hekp page first:
            prefix = await self.bot.get_prefix(ctx.message)
            embed = discord.Embed(
                title=_("Most useful WotBot commands"),
                description=_(
                    """:question:**{0}help** *[command]* - this help or help for a command
:stopwatch:**{0}daily** *[player]* - today's stats
:calendar:**{0}readstats** *[player]* - stats for time period
:tropical_drink:**{0}unicum** *[player]* - carrier stats and goals
:scales:**{0}winrate** *[player]* - quick carrier stats
:popcorn:**{0}denoob** *[player]* - detailed per tier carrier stats and goals

:first_place:**{0}mastery** *[player]* - list of mastered tanks
:chart_with_upwards_trend:**{0}bsig** *[player]* - BlitzStars signature
:play_pause:**{0}replay <url>** - put replay to WotInspector, get details
:game_die:**{0}quiz** - play tank quiz game

:family_wwbb:**{0}clanstats** *[clan]* - clan stats
There are more useful clan commands, see them [here](https://wotbot.pythonanywhere.com/help#ClanStats)

:homes:**{0}invite** - invite WotBot to your server
:tools:**{0}conf** - set your game name, region and other
"""
                ).format(prefix[0]),
                colour=234,
                type="rich",
                url="https://wotbot.pythonanywhere.com/help",
            )
            # embed.set_author(name="b48g55m", url="https://github.com/vanous/", icon_url="http://www.newdesignfile.com/postpic/2015/04/android-icon_321698.png")
            embed.set_footer(
                text="WotBot help",
                icon_url="https://cdn.discordapp.com/avatars/368866137603309569/957dff2d97950943a43127e1561d0409.png",
            )
            # for i in sorted(categ_list):
            # embed.add_field(name='Most used commands', value=":question:**?help [command]** - See this help or help for a command\n2. bbb\n3. ccc\4. ddd",inline=False)
            embed.set_thumbnail(
                url="https://cdn.discordapp.com/avatars/368866137603309569/957dff2d97950943a43127e1561d0409.png"
            )

            try:
                await destination_quick_help.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

            # pages = self.bot.formatter.format_help_for(ctx, self.bot)
            categ_list = {}
            categ_list["Config"] = "Configuration for users and admins"
            for c in self.bot.commands:
                # print(c)
                cmnd = self.bot.get_command(str(c))
                if cmnd.cog_name is not None:
                    # print(type(cmnd).__name__)
                    if inspect.getdoc(cmnd.instance):
                        categ_list[cmnd.cog_name] = inspect.getdoc(cmnd.instance)
            # print(categ_list)

            # embed = discord.Embed(title='WotBot Help', description=""
            #        ,colour=234,type="rich", url="https://vanous.github.io/wotbot/")
            embed = discord.Embed(
                title="WotBot Web Help",
                description="See links for individual command categories below. ",
                colour=234,
                type="rich",
                url="https://wotbot.pythonanywhere.com/help",
            )
            # embed.set_author(name="b48g55m", url="https://github.com/vanous/", icon_url="http://www.newdesignfile.com/postpic/2015/04/android-icon_321698.png")
            embed.set_footer(
                text="WotBot help pages.",
                icon_url="https://cdn.discordapp.com/avatars/368866137603309569/957dff2d97950943a43127e1561d0409.png",
            )
            for i in sorted(categ_list):
                embed.add_field(
                    name="\uFEFF",
                    value="[{}](https://wotbot.pythonanywhere.com/help#{})\n{}".format(
                        i, i, categ_list[i]
                    ),
                    inline=False,
                )
            embed.set_thumbnail(
                url="https://cdn.discordapp.com/avatars/368866137603309569/957dff2d97950943a43127e1561d0409.png"
            )
            try:
                await ctx.message.author.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content="Please enable Embed links permission for wotbot."
                )
            # await self.bot.send_message(destination, embed=embed)
            return
        elif len(commands_) == 1:

            if ctx.message.guild is not None:
                try:
                    await ctx.send(content="See message with help details in PM.")
                except discord.Forbidden:
                    self.bot.logger.warning("Cannot send any message!!!")
                    if ctx.message.guild is not None:
                        self.bot.logger.warning(
                            (ctx.message.guild.name, ctx.message.author.name)
                        )

            # try to see if it is a cog name
            name = bot_module._mention_pattern.sub(repl, commands_[0])
            if name in self.bot.cogs:
                command = self.bot.cogs[name]
            else:
                command = self.bot.all_commands.get(name)
                if command is None:
                    await destination.send(self.bot.command_not_found.format(name))
                    return
            pages = await self.bot.formatter.format_help_for(ctx, command)
        else:
            name = bot_module._mention_pattern.sub(repl, commands_[0])
            command = self.bot.all_commands.get(name)
            if command is None:
                await destination.send(self.bot.command_not_found.format(name))
                return

            for key in commands_[1:]:
                try:
                    key = bot_module._mention_pattern.sub(repl, key)
                    command = command.commands.get(key)
                    if command is None:
                        await destination.send(self.bot.command_not_found.format(key))
                        return
                except AttributeError:
                    await destination.send(
                        self.bot.command_has_no_subcommands.format(command, key)
                    )
                    return

            pages = self.bot.formatter.format_help_for(ctx, command)
        for embed in pages:
            # This has been changed to send embeds
            try:
                await ctx.send(embed=embed)
            except discord.Forbidden:
                try:
                    await ctx.send(
                        content="Sending of help message failed. Either blocked or DM/Embeds are disabled."
                    )
                except discord.Forbidden:
                    if ctx.message.guild is not None:
                        await ctx.send(
                            content="Sending of help message failed. Either blocked or DM/Embeds are disabled."
                        )


def setup(bot):
    bot.add_cog(Utilities(bot))
