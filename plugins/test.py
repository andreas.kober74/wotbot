import discord
from discord.ext import commands
import asyncio
import websockets
import datetime
import random


class Utilities:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(hidden=True)
    async def aaa(self, ctx):
        print("yes")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

    @commands.command(hidden=True)
    # @asyncio.coroutine
    async def sound(self):
        self.mydata = None

        # @asyncio.coroutine
        async def echo(websocket, path):
            # data = await on_voice_state_update()
            while True:
                if self.mydata is not None:
                    data = str(self.mydata)
                    print("mam data")
                    self.mydata = None
                    await websocket.send(data)
                else:
                    await asyncio.sleep(0.5)

        # @asyncio.coroutine
        async def time(websocket, path):
            while True:
                now = datetime.datetime.utcnow().isoformat() + "Z"
                await websocket.send(now)
                await asyncio.sleep(random.random() * 3)

        # @asyncio.coroutine
        async def start():
            await websockets.serve(echo, "192.168.3.61", 10000, loop=self.bot.loop)

        @self.bot.event
        # @asyncio.coroutine
        async def on_voice_state_update(before, after):
            print(
                after.server,
                after,
                after.voice.voice_channel,
                after.voice.voice_channel.id,
                after.voice.voice_channel.voice_members,
                after.self_mute,
            )
            if after.id == "345299188986150923":
                if after.self_mute is True:
                    self.mydata = "Muted"
                else:
                    self.mydata = "On AIR"
            # self.bot.dispatch("mujevent",after)

        await start()

    @commands.command(pass_context=True, hidden=True)
    # @asyncio.coroutine
    async def talk(self, ctx, msg):
        """Is this ID bot?"""
        await ctx.send(msg)
        pass

    # @commands.command(parent="Utilities", hidden=True)
    # @asyncio.coroutine
    # async def ttt(self):
    #    js = await r.json()
    #    await r.release()
    #    await ctx.send(js["nickname"])
    @commands.command(pass_context=True, parent="Utilities", hidden=True)
    # @asyncio.coroutine
    async def test(self, ctx, url=None, *, title=None):
        print(url)
        print("title:", title)
        embed = discord.Embed(
            title="title",
            colour=discord.Colour(0x453BD3),
            url="https://discordapp.com",
            description="this supports [named links](https://discordapp.com) on top of the previously shown subset of markdown.",
        )
        # embed = discord.Embed(title="title ~~(did you know you can have markdown here too?)~~", colour=discord.Colour(0x453bd3), url="https://discordapp.com", description="this supports [named links](https://discordapp.com) on top of the previously shown subset of markdown. ```\nyes, even code blocks```", timestamp=datetime.datetime.utcfromtimestamp(1511804660))
        # embed.set_image(url="https://cdn.discordapp.com/embed/avatars/0.png")
        # embed.set_thumbnail(url="https://cdn.discordapp.com/embed/avatars/0.png")
        # embed.set_author(name="author name", url="https://discordapp.com", icon_url="https://cdn.discordapp.com/embed/avatars/0.png")
        # embed.set_footer(text="footer text", icon_url="https://cdn.discordapp.com/embed/avatars/0.png")
        # embed.add_field(name="ia", value="some of these properties have certain limits...")
        # embed.add_field(name="a", value="try exceeding some of them!")
        # embed.add_field(name="a", value="an informative error should show up, and this view will remain as-is until all issues are fixed")
        # embed.add_field(name="<:thonkang:219069250692841473>", value="these last two", inline=True)
        # embed.add_field(name="<:thonkang:219069250692841473>", value="are inline fields", inline=True)
        # await ctx.send(content="this `supports` __a__ **subset** *of* ~~markdown~~  ```js\nfunction foo(bar) {\n  console.log(bar);\n}\n\nfoo(1);```", embed=embed)
        await ctx.send(content=None, embed=embed)


def setup(bot):
    bot.add_cog(Utilities(bot))
