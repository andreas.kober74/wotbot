import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
import aiohttp


# unicode list here: http://www.unicode.org/charts/PDF/U1F300.pdf

reload_emoji = "\U0001F512"
calc_emoji = "\U0001F522"


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False, aliases=["win-rate", "wr", "ps"])
    # @asyncio.coroutine
    async def winrate(self, ctx, *, player_name: str = None):
        """See per tier stats. Battles, WR and DMG."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        # all_vehicles=self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier")

        all_vehicles = await self.bot.wg.get_all_vehicles()

        if player_name is None or ctx.message.mentions:
            player_name = await self.bot.wg.get_local_name(ctx)
        self.bot.logger.info(("winrate:", player_name))

        player, region = await self.bot.wg.get_player_a(player_name, ctx.message)
        # print("xxx",player, region)
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]

            all_tiers = {}
            result_total = Counter()
            print_tiers = []
            # return self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles,all.damage_dealt")[player_id]
            player_tanks = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="stats",
                parameter="account_id={}&fields=tank_id,all.wins,all.battles,all.damage_dealt".format(
                    player_id
                ),
            )
            # print("iii",player_tanks)
            player_tanks = player_tanks[player_id]
            # player_tanks=await self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles,all.damage_dealt")[player_id]
            # print(player_tanks)
            if player_tanks:
                for tank in player_tanks:
                    if str(tank["tank_id"]) in all_vehicles:
                        tier = all_vehicles[str(tank["tank_id"])]["tier"]
                    else:
                        tier = 0
                    if tier not in all_tiers:
                        all_tiers[tier] = tank["all"]
                    else:
                        temp = dict(Counter(all_tiers[tier]) + Counter(tank["all"]))
                        all_tiers[tier] = temp
                    result_total.update(tank["all"])
            else:
                await ctx.send(
                    content=_("No tanks data for player `{}@{}`").format(
                        player_nick, region
                    )
                )
                return True

            # self.bot.logger.debug(all_tiers)
            # self.bot.logger.debug(result_total)

            player_rating, player_rating_league_icon, player_rating_league = await self.bot.wg.get_rating(
                player_id, region, ctx
            )
            stats_rating = None

            info_data_new = await self.bot.wg.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="info",
                parameter="account_id={}&extra=statistics.rating".format(player_id),
            )

            if not info_data_new:
                await ctx.send(
                    content=_("No account data from WG for player `{}@{}`").format(
                        player_nick, region
                    )
                )
                return True

            info_data_new = info_data_new[player_id].get("statistics", None)
            if info_data_new:
                info_data_new = info_data_new.get("rating", None)
            if info_data_new:
                # print(info_data_new)
                rating_data = {}
                rating_data["battles"] = info_data_new.get("battles", "n/a")
                try:
                    rating_data["dpb"] = round(
                        info_data_new["damage_dealt"] / float(info_data_new["battles"]),
                        2,
                    )
                except ArithmeticError as e:
                    rating_data["dpb"] = "n/a"
                try:
                    rating_data["winrate"] = round(
                        info_data_new["wins"] / float(info_data_new["battles"]) * 100, 2
                    )
                except ArithmeticError as e:
                    rating_data["winrate"] = "n/a"

            self.bot.logger.debug(all_tiers)
            for i in all_tiers:
                try:
                    print_tiers.append(
                        [
                            i,
                            all_tiers[i]["battles"],
                            all_tiers[i]["wins"] / all_tiers[i]["battles"] * 100,
                            all_tiers[i]["damage_dealt"] / all_tiers[i]["battles"],
                        ]
                    )
                except:
                    print("No data for tier", i, all_tiers)

            print_tiers = sorted(print_tiers, key=lambda x: (x[0], x[3]))

            try:
                print_tiers.append(
                    [
                        _("Total:"),
                        result_total["battles"],
                        result_total["wins"] / result_total["battles"] * 100,
                        result_total["damage_dealt"] / result_total["battles"],
                    ]
                )
            except:
                print_tiers.append(
                    [
                        _("Total:"),
                        result_total["battles"],
                        result_total["wins"],
                        result_total["damage_dealt"],
                    ]
                )

            # toutotal="{}\nRating: {}\n".format(tabulate(print_tiers,headers=["Tier","Battles","WR", "DMG"], floatfmt=".2f",numalign="right"),player_rating)

            toutotal = "{}\n".format(
                tabulate(
                    print_tiers,
                    headers=[_("Tier"), _("Battles"), _("WR"), _("DMG")],
                    floatfmt=".2f",
                    numalign="right",
                )
            )

            embed = discord.Embed(
                title=_("Winrate for `{0}@{1}`:").format(player_nick, region),
                description="```{}```".format(toutotal),
                colour=234,
                type="rich",
            )
            embed.add_field(
                name=_("Rating, current season:"),
                value="```{}{}```".format(player_rating_league, player_rating),
            )
            if rating_data:
                # print(rating_data)
                embed.add_field(
                    name=_("Rating, total stats:"),
                    value=_("```Battles: {battles} WR: {winrate} DpB: {dpb}```").format(
                        **rating_data
                    ),
                    inline=False,
                )

            # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
            if player_rating_league_icon is not None:
                embed.set_thumbnail(url=player_rating_league_icon)
            else:
                embed.set_thumbnail(
                    url="https://wotblitz.com/newstatic/images/twister_icon.png"
                )

            try:
                msg = await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    _("Please enable Embed links permission for wotbot.")
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
