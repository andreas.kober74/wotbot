import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
import aiohttp


class UserStats:
    """User statistics"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True, hidden=True, aliases=["savestatsm", "savestatsmulti"]
    )
    # @asyncio.coroutine
    async def savestatsmany(self, ctx, *, players_names: str = None):
        """Make a checkpoint of tier stats. From 0 (your name) to 10 WG names."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        # all_vehicles=self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier")
        all_vehicles = await self.bot.wg.get_all_vehicles()

        if players_names is None:
            players_names = self.bot.wg.get_local_name(ctx)
        self.bot.logger.info(("savestatsmany:", players_names))

        for player_name in players_names.split():
            player, region = await self.bot.wg.get_player_a(player_name, ctx.message)
            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]

                all_tiers = {}
                result_total = Counter()
                print_tiers = []
                # player_tanks=self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles, all.damage_dealt")[player_id]
                player_tanks = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="tanks",
                    cmd_path="stats",
                    parameter="account_id={}&fields=tank_id, all.wins, all.battles, all.damage_dealt".format(
                        player_id
                    ),
                )
                player_tanks = player_tanks[player_id]
                for tank in player_tanks:
                    if str(tank["tank_id"]) in all_vehicles:
                        tier = all_vehicles[str(tank["tank_id"])]["tier"]
                    else:
                        tier = 0
                    if tier not in all_tiers:
                        all_tiers[tier] = tank["all"]
                    else:
                        temp = dict(Counter(all_tiers[tier]) + Counter(tank["all"]))
                        all_tiers[tier] = temp
                    result_total.update(tank["all"])

                player_rating, player_rating_league_icon, player_rating_league = await self.bot.wg.get_rating(
                    player_id, region, ctx, score=1
                )

                self.bot.logger.debug(all_tiers)
                for i in all_tiers:
                    # print_tiers.append([i,all_tiers[i]["battles"],all_tiers[i]["wins"]/all_tiers[i]["battles"]*100])
                    print_tiers.append(
                        [
                            i,
                            all_tiers[i]["battles"],
                            all_tiers[i]["wins"] / all_tiers[i]["battles"] * 100,
                            all_tiers[i]["damage_dealt"] / all_tiers[i]["battles"],
                        ]
                    )
                with open(
                    "./stats/{}-{}.pickle".format(ctx.message.author.id, player_id),
                    "wb",
                ) as storage:
                    pickle.dump(
                        [datetime.datetime.now(), all_tiers, {"rating": player_rating}],
                        storage,
                    )

                # toutotal="{}\nRating: {}\n".format(tabulate(print_tiers,headers=["Tier","Battles","WR"], floatfmt=".2f",numalign="right"),player_rating)
                print_tiers = sorted(print_tiers, key=lambda x: (x[0], x[3]))
                print_tiers.append(
                    [
                        "Total:",
                        result_total["battles"],
                        result_total["wins"] / result_total["battles"] * 100,
                        result_total["damage_dealt"] / result_total["battles"],
                    ]
                )
                toutotal = "{}\nRating: {}\n".format(
                    tabulate(
                        print_tiers,
                        headers=["Tier", "Battles", "WR", "DMG"],
                        floatfmt=".2f",
                        numalign="right",
                    ),
                    player_rating,
                )

                embed = discord.Embed(
                    title="Saving data for `{0}@{1}`. Get tier stats diff by using ?statsmany `{0}@{1}`".format(
                        player_nick, region
                    ),
                    description="```{}```".format(toutotal),
                    colour=234,
                    type="rich",
                )
                # embed.add_field(name='\uFEFF', value="```{}```".format(o))
                # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
                embed.set_footer(text="Check stats after few battles.")
                if player_rating_league_icon is not None:
                    embed.set_thumbnail(url=player_rating_league_icon)
                else:
                    embed.set_thumbnail(
                        url="https://wotblitz.com/newstatic/images/twister_icon.png"
                    )
                try:
                    await ctx.send(content=None, embed=embed)
                except discord.Forbidden:
                    self.bot.logger.warning(
                        "Please enable Embed links permission for wotbot."
                    )
                    await ctx.send(
                        content="Please enable Embed links permission for wotbot."
                    )
                    break
            else:
                out = await self.bot.wg.search_player_a(ctx, player_name)
                await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
