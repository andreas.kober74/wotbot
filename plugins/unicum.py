import discord
from discord.ext import commands

# import asyncio
from operator import itemgetter
import random

# import requests
import json
import math
from tabulate import tabulate
import aiohttp

# http://forum.wotblitz.com/index.php?/topic/33398-guide-calculating-battles-needed-to-reach-a-desired-winrate/
# http://forum.wotblitz.com/index.php?/topic/33554-advanced-equation-for-finding-battles-needed-to-achieve-certain-wr/
# http://www.wot-fightclub.de/wot/road_to_victory/calc.html


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False, aliases=["unikum"])
    # @asyncio.coroutine
    async def unicum(self, ctx, *, player_name: str = None):
        """Road to Unicum - see when you get there."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        if player_name is None or ctx.message.mentions:
            player_name = await self.bot.wg.get_local_name(ctx)
        self.bot.logger.info(("unicum:", player_name))

        player, region = await self.bot.wg.get_player_a(player_name, ctx.message)
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]
            url = "https://www.blitzstars.com/api/top/player/{}".format(player_id)
            async with aiohttp.ClientSession(loop=self.bot.loop) as client:
                async with client.get(url) as r:
                    data = None
                    if r.status == 200:
                        data = await r.json(content_type=None)
                        if (data) and (len(data) > 0):
                            self.bot.logger.debug(data)
                        else:
                            await ctx.send(_("No data on BlitzStars"))
                            return
            if data is not None:
                # get WG data for fresher winrate/battle count
                # member_info=self.bot.wg.wotb_servers[region].account.info(account_id=player_id)[player_id]
                member_info = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="account",
                    cmd_path="info",
                    parameter="account_id={}".format(player_id),
                )
                member_info = member_info[player_id]
                if data["period90d"] is None:
                    await ctx.send(_("No data on BlitzStars"))
                    return

                else:
                    if not data["period90d"].get("all", False):
                        await ctx.send(_("No data on BlitzStars"))
                        return

                    wr90 = data["period90d"]["special"]["winrate"]
                    dpb90 = data["period90d"]["special"]["dpb"]
                    battles_90 = data["period90d"]["all"]["battles"]
                    bpd_90 = battles_90 / 90

                if data["statistics"] is None:
                    await ctx.send(_("No data on BlitzStars"))
                    return

                # wr=data["statistics"]["special"]["winrate"]
                # battles=data["statistics"]["all"]["battles"]
                if member_info is not None:
                    battles = member_info["statistics"]["all"]["battles"]
                    damage = member_info["statistics"]["all"]["damage_dealt"]
                    wins = member_info["statistics"]["all"]["wins"]
                    wr = wins / battles * 100
                    dpb = damage / battles

                else:
                    await ctx.send(_("No data on BlitzStars"))
                    return

                try:
                    shortgoal = math.ceil(wr)
                    midgoal = shortgoal + 5
                    shortbattles90 = math.ceil(
                        (battles * (shortgoal - wr)) / (wr90 - shortgoal)
                    )
                    shortbattles100 = math.ceil(
                        (battles * (shortgoal - wr)) / (100 - shortgoal)
                    )
                    midbattles90 = math.ceil(
                        (battles * (midgoal - wr)) / (wr90 - midgoal)
                    )
                    midbattles100 = math.ceil(
                        (battles * (midgoal - wr)) / (100 - midgoal)
                    )

                    shortgoal_dpb = int(math.ceil(dpb / 100.0)) * 100
                    midgoal_dpb = shortgoal_dpb + 100
                    shortbattles90_dpb = math.ceil(
                        (battles * (shortgoal_dpb - dpb)) / (dpb90 - shortgoal_dpb)
                    )
                    midbattles90_dpb = math.ceil(
                        (battles * (midgoal_dpb - dpb)) / (dpb90 - midgoal_dpb)
                    )

                except ArithmeticError as e:
                    await ctx.send(
                        _(
                            "Cannot calculate due to ArithmeticError: {}. One of goals ({}, {}) is the same as one of winrates ({}, 100). Try again in near future, when your stats change a bit. Also, try the ?denoob command, to see same stats but per tier."
                        ).format(e, shortgoal, midgoal, wr90)
                    )
                    return True

                # ((Total Battles Now x Target/Goal WR) - (Total # of Wins Right Now * 100)) / (Recent WR - Target/Goal WR)
                # var battles_needed_for_WR = Math.ceil((battle_count * (target_WR - old_WR ) ) / (current_WR - target_WR ));
                toutotal = _(
                    "To get to {} wr (+{:.2f}%), you {} with current 90day wr of {} {} can play {} battles with 100% winrate.\n\n"
                ).format(
                    shortgoal,
                    shortgoal - wr,
                    _(
                        "need to play {} battles, (this would take you about {} days with your average of {} battles per day),"
                    ).format(
                        shortbattles90,
                        math.ceil(shortbattles90 / bpd_90),
                        math.ceil(bpd_90),
                    )
                    if shortbattles90 > 0
                    else _("cannot reach"),
                    wr90,
                    _("or") if shortbattles90 > 0 else _("but"),
                    shortbattles100,
                )
                toutotal += _(
                    "To get to {} wr (+{:.2f}%), you {} with current 90day wr of {} {} can play {} battles with 100% winrate."
                ).format(
                    midgoal,
                    midgoal - wr,
                    _("need to play {} battles").format(midbattles90)
                    if midbattles90 > 0
                    else _("cannot reach"),
                    wr90,
                    _("or") if midbattles90 > 0 else _("but"),
                    midbattles100,
                )
                toutotal_dpb = _(
                    "To get to {} dpb (+{:.0f}), you {} with current 90day dpb of {:.2f}\u00A0.\n\n"
                ).format(
                    shortgoal_dpb,
                    shortgoal_dpb - dpb,
                    _("need to play {} battles").format(shortbattles90_dpb)
                    if shortbattles90_dpb > 0
                    else _("cannot reach"),
                    dpb90,
                )
                toutotal_dpb += _(
                    "To get to {} dpb (+{:.0f}), you {} with current 90day dpb of {:.2f}\u00A0."
                ).format(
                    midgoal_dpb,
                    midgoal_dpb - dpb,
                    _("need to play {} battles").format(midbattles90_dpb)
                    if midbattles90_dpb > 0
                    else _("cannot reach"),
                    dpb90,
                )

            # toutotal+="{}\n\n".format(tabulate(head,floatfmt=".2f",numalign="right"))
            else:
                await ctx.send(_("No data"))
                return
            self.bot.logger.debug(len(str(toutotal)))
            embed = discord.Embed(
                title=_("`{}@{}` UNICUM yet?").format(player_nick, region),
                colour=234,
                type="rich",
                url="https://www.blitzstars.com/player/{}/{}".format(
                    region, player_nick
                ),
            )
            embed.add_field(
                name=_("Current stats"),
                value=_("WinRate: {:.2f}, Damage per Battle: {:.2f}").format(wr, dpb),
            )
            embed.add_field(name=_("Winrate:"), value="```{}```".format(toutotal))
            embed.add_field(
                name=_("Damage per battle:"), value="```{}```".format(toutotal_dpb)
            )
            # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
            embed.set_thumbnail(
                url="https://user-images.githubusercontent.com/3680926/33225882-8d69eda8-d180-11e7-8e00-3a4fd2a32821.png"
            )
            embed.set_footer(
                text="90day wr/dpb  data by BlitzStars.",
                icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png",
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    _("Please enable Embed links permission for wotbot.")
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
