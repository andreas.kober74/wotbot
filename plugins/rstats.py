import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
from pathlib import Path
import humanize
import aiohttp
import math


percents = {
    0.0: "https://wotblitz.com/newstatic/images/twister_icon.png",
    1.0: "https://user-images.githubusercontent.com/3680926/34320161-ae8db030-e7f3-11e7-8e09-442ce9c41bde.png",
    2.0: "https://user-images.githubusercontent.com/3680926/34320162-aeb1d028-e7f3-11e7-8ff5-54e1646991a6.png",
    3.0: "https://user-images.githubusercontent.com/3680926/34320163-aeda1452-e7f3-11e7-9bc7-eae519371a5f.png",
    4.0: "https://user-images.githubusercontent.com/3680926/34320165-aef8bca4-e7f3-11e7-8f0e-c25f9554c7e8.png",
    5.0: "https://user-images.githubusercontent.com/3680926/34320166-af1bad9a-e7f3-11e7-9dc3-317b4069ddf3.png",
    6.0: "https://user-images.githubusercontent.com/3680926/34320167-af4208a0-e7f3-11e7-94f0-6f88c058596e.png",
    7.0: "https://user-images.githubusercontent.com/3680926/34320168-af623576-e7f3-11e7-93b5-b21ce29752b7.png",
    8.0: "https://user-images.githubusercontent.com/3680926/34320169-af805056-e7f3-11e7-9872-31ecfa802229.png",
    9.0: "https://user-images.githubusercontent.com/3680926/34320170-af9e6492-e7f3-11e7-944c-b1d480623d6e.png",
    10.0: "https://user-images.githubusercontent.com/3680926/34318931-d9e3abce-e7d3-11e7-9f27-ae725c0cf109.png",
}

# unicode list here: http://www.unicode.org/charts/PDF/U1F300.pdf

reload_emoji = "\U0001F512"
calc_emoji = "\U0001F522"


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True, hidden=False, aliases=["readstat", "userstats"]
    )
    # @asyncio.coroutine
    async def readstats(self, ctx, *, player_name: str = None):
        """Show single player tier stats difference since last checkpoint. You can make new checkpoint at any time with savestats."""
        autorun = False
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        # all_vehicles=dict(self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier"))
        # all_vehicles['64081']= {'tier': 1, 'name': 'mk1 heavy'}
        # all_vehicles['56609']= {'tier': 7, 'name': 'T49A'}
        # all_vehicles['12545']= {'tier': 9, 'name': 'K-91'}
        all_vehicles = await self.bot.wg.get_all_vehicles()

        if player_name is None or ctx.message.mentions:
            player_name = await self.bot.wg.get_local_name(ctx)
        self.bot.logger.info(("readstats:", player_name))

        player, region = await self.bot.wg.get_player_a(player_name, ctx.message)
        # self.bot.logger.debug(len(player))
        # self.bot.logger.debug(player)
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]

            all_tiers = {}
            # player_tanks=self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles, all.damage_dealt")[player_id]
            player_tanks = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="stats",
                parameter="account_id={}&fields=tank_id, all.wins, all.battles, all.damage_dealt".format(
                    player_id
                ),
            )
            player_tanks = player_tanks[player_id]
            if player_tanks is None:
                await ctx.send("No data for `{}@{}` ".format(player_nick, region))
                return
            for tank in player_tanks:
                if str(tank["tank_id"]) in all_vehicles:
                    tier = all_vehicles[str(tank["tank_id"])]["tier"]
                else:
                    tier = 0
                    print("not found", tank, str(tank["tank_id"]))
                if tier not in all_tiers:
                    all_tiers[tier] = tank["all"]
                else:
                    all_tiers[tier] = dict(
                        Counter(all_tiers[tier]) + Counter(tank["all"])
                    )
            self.bot.logger.debug(all_tiers)
            # print("all tiers:",all_tiers)

            player_rating, player_rating_league_icon, player_rating_league = await self.bot.wg.get_rating(
                player_id, region, ctx, score=1
            )

            wr_ttl = None
            old_tiers = False
            print("player id:", player_id)
            my_file = Path(
                "./stats/{}-{}.pickle".format(ctx.message.author.id, player_id)
            )
            if my_file.is_file():
                with open(
                    "./stats/{}-{}.pickle".format(ctx.message.author.id, player_id),
                    "rb",
                ) as storage:
                    old_tiers = pickle.load(storage)
            else:
                my_file = Path("./stats/{}.pickle".format(player_id))
                if my_file.is_file():
                    with open("./stats/{}.pickle".format(player_id), "rb") as storage:
                        old_tiers = pickle.load(storage)
            if old_tiers:
                ago = datetime.datetime.now() - old_tiers[0]
                ago = humanize.naturaltime(ago)
                ago = "Reading data for `{}@{}`, saved {}.".format(
                    player_nick, region, ago
                )
                # self.bot.logger.debug(old_tiers[1])
                player_rating_old = None
                if len(old_tiers) > 2:
                    player_rating_old = old_tiers[2].get("rating", 0)
                # self.bot.logger.debug(("old rating:", player_rating_old))

                damage_total = True
                if player_rating_old is not None and player_rating is not None:
                    if isinstance(player_rating_old, str):
                        player_rating = "{}, try re-save".format(player_rating_old)
                    elif isinstance(player_rating, str):
                        player_rating = "{}, try re-save".format(player_rating)
                    else:
                        player_rating = player_rating - player_rating_old
                else:
                    player_rating = "unsaved yet"

                for j in old_tiers[1]:
                    if "damage_dealt" not in old_tiers[1][j]:
                        old_tiers[1][j]["damage_dealt"] = all_tiers[j]["damage_dealt"]
                        damage_total = False

                self.bot.logger.debug(old_tiers)
                # print(old_tiers)

                result_tier = []
                result_total = Counter()
                results_improve = ""
                for i in all_tiers:
                    # self.bot.logger.debug((Counter(all_tiers[i]), Counter(old_tiers[1].get(i,{'battles': 0, 'wins': 0,'damage_dealt':0}))))
                    temp = Counter(all_tiers[i]) - Counter(
                        old_tiers[1].get(
                            i, {"battles": 0, "wins": 0, "damage_dealt": 0}
                        )
                    )
                    if temp["battles"] > 0:
                        # temp=temp+Counter({'battles': 0, 'wins': 0,'damage_dealt':0})
                        temp = Counter(all_tiers[i])
                        temp.subtract(
                            Counter(
                                old_tiers[1].get(
                                    i, {"battles": 0, "wins": 0, "damage_dealt": 0}
                                )
                            )
                        )

                    # self.bot.logger.debug((i, temp))

                    result_total.update(temp)
                    if temp:
                        try:
                            result_tier.append(
                                [
                                    i,
                                    temp["battles"],
                                    temp["wins"] / temp["battles"] * 100,
                                    temp["damage_dealt"] / temp["battles"],
                                ]
                            )
                        except:
                            result_tier.append([i, temp["battles"], "0", "0"])
                self.bot.logger.debug(result_total)
                if len(result_tier) > 0:
                    if damage_total:
                        foot = "Click {} to view stats. Click {} or type ?savestats to make new checkpoint. Due to changes in update 5.5, some tiers might show strange results, ?savestats is recommended.".format(
                            calc_emoji, reload_emoji
                        )
                    else:
                        foot = "Some tanks have unsaved DMG, calculated damages are not accurate. Re-save is recommended. Click {} to view stats. Click {} or type ?savestats to make new checkpoint.".format(
                            calc_emoji, reload_emoji
                        )
                    result_tier = sorted(result_tier, key=lambda x: (x[0], x[3]))
                    result_tier.append(
                        [
                            "Total:",
                            result_total["battles"],
                            result_total["wins"] / result_total["battles"] * 100,
                            result_total["damage_dealt"] / result_total["battles"],
                        ]
                    )
                    wr_ttl = result_total["wins"] / result_total["battles"] * 100
                    if wr_ttl < 60:
                        results_improve += "Improve to:\n"
                        results_improve += "61%, play {} battles with 70%WR.\n".format(
                            math.ceil(
                                (result_total["battles"] * (61 - wr_ttl)) / (70 - 61)
                            )
                        )
                        results_improve += "61%, play {} battles with 75%WR.\n".format(
                            math.ceil(
                                (result_total["battles"] * (61 - wr_ttl)) / (75 - 61)
                            )
                        )
                        results_improve += "61%, play {} battles with 85%WR.\n".format(
                            math.ceil(
                                (result_total["battles"] * (61 - wr_ttl)) / (85 - 61)
                            )
                        )
                        results_improve += "61%, play {} battles with 100%WR.\n".format(
                            math.ceil(
                                (result_total["battles"] * (61 - wr_ttl)) / (100 - 61)
                            )
                        )
                        results_improve += "65%, play {} battles with 70%WR.\n".format(
                            math.ceil(
                                (result_total["battles"] * (65 - wr_ttl)) / (70 - 65)
                            )
                        )
                        results_improve += "65%, play {} battles with 75%WR.\n".format(
                            math.ceil(
                                (result_total["battles"] * (65 - wr_ttl)) / (75 - 65)
                            )
                        )
                        results_improve += "65%, play {} battles with 85%WR.\n".format(
                            math.ceil(
                                (result_total["battles"] * (65 - wr_ttl)) / (85 - 65)
                            )
                        )
                        results_improve += "65%, play {} battles with 100%WR.\n".format(
                            math.ceil(
                                (result_total["battles"] * (65 - wr_ttl)) / (100 - 65)
                            )
                        )
                        # ((Total Battles Now x Target/Goal WR) - (Total # of Wins Right Now * 100)) / (Recent WR - Target/Goal WR)

                    toutotal = "{}\n{}\nRating diff: {}\n".format(
                        tabulate(
                            result_tier,
                            headers=["Tier", "Battles", "WR", "DMG"],
                            floatfmt=".2f",
                            stralign="right",
                            numalign="right",
                        ),
                        results_improve,
                        player_rating,
                    )
                else:
                    toutotal = "{}\nRating diff: {}\n".format(
                        "No battles since checkpoint.", player_rating
                    )
                    foot = "To view stats click {} to reset click {} No data right now.".format(
                        calc_emoji, reload_emoji
                    )

            else:
                ago = "No data for `{}@{}`. First make a checkpoint:".format(
                    player_nick, region
                )
                toutotal = "?savestats"
                foot = "Let me try to do it for you right now..."
                autorun = True
                # self.bot.dispatch('command',command,ctx)
                # await command.invoke(ctx)

            embed = discord.Embed(
                title=ago,
                description="```{}```".format(toutotal),
                colour=234,
                type="rich",
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
            embed.set_footer(text="{}".format(foot))
            if wr_ttl is not None:
                embed.set_thumbnail(url=percents[round(wr_ttl, -1) / 10])
            try:
                msg = await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content="Please enable Embed links permission for wotbot."
                )
            try:
                await msg.add_reaction(calc_emoji)  # test of votable emoji → "reaction"
                await msg.add_reaction(
                    reload_emoji
                )  # test of votable emoji → "reaction"
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Add reactions permission for wotbot."
                )
                await ctx.send(
                    content="Please enable Embed links permission for wotbot."
                )

            if autorun:
                self.bot.logger.debug("autorun")
                # await self.bot.delete_message(msg)
                command = self.bot.all_commands["savestats"]
                await ctx.invoke(command, player_name=player_nick)
            await asyncio.sleep(0.1)

            # res=await self.bot.wait_for_reaction([reload_emoji,calc_emoji], message=msg,user=ctx.message.author)
            def check(reaction, user):
                # print("checking rstats",user.name,ctx.message.author.name,msg.author.name)
                # print("checking rstats",user.id,ctx.message.author.id,msg.author.id)
                if msg.id == reaction.message.id:
                    if user != msg.author:
                        return reaction, user

            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", check=check, timeout=300
                )
            except asyncio.TimeoutError:
                print("timed out")
                try:
                    await msg.remove_reaction(calc_emoji, msg.author)
                    await msg.remove_reaction(reload_emoji, msg.author)
                except Exception as e:
                    self.bot.logger.info(e)
                return

            # self.bot.logger.debug(res.reaction.message.author)
            # self.bot.logger.debug(res.user)
            if reaction.emoji == reload_emoji and ctx.message.author.id == user.id:
                command = self.bot.all_commands["savestats"]
                await ctx.invoke(command, player_name=player_nick)
            elif reaction.emoji == calc_emoji:
                command = self.bot.all_commands["readstats"]
                await ctx.invoke(command, player_name=player_nick)
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
