import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
import math
from tabulate import tabulate
import humanize
import time


class Utilities:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        hidden=False,
        aliases=["serverinfo", "infoserver", "server", "serverpopulation"],
    )
    # @asyncio.coroutine
    async def servers(self, ctx):
        """See current number of players per region."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        tstart = time.time()
        self.bot.logger.info("servers")
        players_total = await self.bot.wg.get_players_count()
        embed = discord.Embed(
            title=_("Server status").format(), description=None, colour=234, type="rich"
        )
        embed.add_field(
            name="\uFEFF",
            value="```{}```".format(tabulate(players_total, headers="keys")),
            inline=False,
        )
        embed.set_thumbnail(
            url="https://cdn.discordapp.com/attachments/370945953550696449/479506499199172609/cup-icon1.png"
        )
        embed.set_footer(
            text="Number of players per region",
            icon_url="https://cdn.discordapp.com/attachments/370945953550696449/479505184507035648/wargaming-logo.png",
        )
        try:
            await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning(
                _("Please enable Embed links permission for wotbot.")
            )
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )
        print(humanize.naturaldelta(tstart - time.time()))


def setup(bot):
    bot.add_cog(Utilities(bot))
