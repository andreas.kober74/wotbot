import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
from pathlib import Path
import humanize

ppl_to_wg = {"m": "4", "a": "4", "ace": "4", "1": "3", "2": "2", "3": "1", "0": "0"}


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False, aliases=["master", "masteries"])
    # @asyncio.coroutine
    async def mastery(self, ctx, mark: str = None, player_name: str = None):
        """Show list of tanks with Mark Of Mastery. Choose type of mastery: ace, 1, 2, 3, 0, default is ace."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        autorun = False
        mastery_marks = {
            "4": _("Ace Tanker"),
            "3": _("1st Class"),
            "2": _("2nd Class"),
            "1": _("3rd Class"),
            "0": _("No mastery"),
        }

        # all_vehicles=dict(self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier,name"))
        # print(all_vehicles)
        # all_vehicles['64081']= {'tier': 1, 'name': 'mk1 heavy'}
        # all_vehicles['56609']= {'tier': 7, 'name': 'T49A'}
        # all_vehicles['12545']= {'tier': 9, 'name': 'K-91'}

        all_vehicles = await self.bot.wg.get_all_vehicles()
        if player_name is None or ctx.message.mentions:
            player_name = await self.bot.wg.get_local_name(ctx)

        self.bot.logger.info(("mastery:", player_name, mark))
        if mark is None:
            mark = "4"
        else:
            if len(mark) > 3:
                player_name = mark
                mark = "4"
            else:
                try:
                    int(mark)
                    mark = ppl_to_wg[mark.lower()]
                except:
                    mark = "4"
        player, region = await self.bot.wg.get_player_a(player_name, ctx.message)
        # self.bot.logger.debug((player_name,mark))
        # self.bot.logger.debug((player, region))
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]
            # self.bot.logger.debug(player_id)

            all_tanks = []
            # player_tanks=self.bot.wg.wotb_servers[region].tanks.achievements(account_id=player_id, fields="tank_id,achievements")[player_id]
            # player_tanks = await self.bot.wg.get_wg(region=region,cmd_group="tanks",cmd_path="achievements",parameter="account_id={}&fields=tank_id,achievements".format(player_id))
            player_tanks = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="stats",
                parameter="account_id={}&fields=tank_id,mark_of_mastery".format(
                    player_id
                ),
            )
            player_tanks = player_tanks[player_id]
            if player_tanks:
                for tank in player_tanks:
                    if str(tank["tank_id"]) in all_vehicles:
                        markOfMastery = str(tank.get("mark_of_mastery", "0"))
                        if markOfMastery == mark:
                            tank_name = all_vehicles[str(tank["tank_id"])]["name"]
                            tank_tier = all_vehicles[str(tank["tank_id"])]["tier"]
                            all_tanks.append([tank_tier, tank_name])
                    else:
                        print("tank missing in tankopedia:", tank)
                        tank_name = _("Not in tankopedia")
                        tank_tier = 0
                        all_tanks.append([tank_tier, tank_name])
                all_tanks_sorted = sorted(all_tanks, key=itemgetter(0), reverse=True)
                self.bot.logger.debug(len(all_tanks_sorted))
                for u in range(0, len(all_tanks_sorted), 100):
                    toutotal = "{}\n".format(
                        tabulate(
                            all_tanks_sorted[u : u + 100],
                            floatfmt=".2f",
                            stralign="left",
                            numalign="right",
                        )
                    )
                    self.bot.logger.debug(len(str(toutotal)))
                    embed = discord.Embed(
                        title=_("Mastery list of `{}@{}` for {}").format(
                            player_nick, region, mastery_marks[mark]
                        ),
                        description=_("```Tier/Tank\n{}```").format(toutotal),
                        colour=234,
                        type="rich",
                    )
                    # embed.add_field(name='\uFEFF', value="```{}```".format(o))
                    # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
                    embed.set_thumbnail(
                        url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/big/markOfMastery{}.png".format(
                            mark
                        )
                    )
                    # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(mark))
                    embed.set_footer(
                        text=_(
                            "Good job, {} of {}.\n Tier 0 are tanks not in WG tankopedia."
                        ).format(len(all_tanks), mastery_marks[mark]),
                        icon_url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(
                            mark
                        ),
                    )
                    try:
                        await ctx.send(content=None, embed=embed)
                    except discord.Forbidden:
                        self.bot.logger.warning(
                            "Please enable Embed links permission for wotbot."
                        )
                        await ctx.send(
                            content=_(
                                "Please enable Embed links permission for wotbot."
                            )
                        )
            else:
                await ctx.send(_("No data."))
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
