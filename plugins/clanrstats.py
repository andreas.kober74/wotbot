import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import datetime
import pickle
from pathlib import Path
from collections import Counter
import humanize
import math
import humanize
import time


class ClanStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        aliases=[
            "clanreadstats",
            "readclanstats",
            "readclan",
            "clanread",
            "calnreadstats",
            "readcalnstats",
            "readcaln",
            "clanstata",
            "calnstata",
            "calnread",
            "clan-stats",
        ],
    )
    # @asyncio.coroutine
    async def clanstats(self, ctx, *, clan_name: str = None):
        """Clan stats since last checkpoint. Clan name or empty for current user's clan. Make checkpoint at anytime with clansavestats."""
        self.bot.logger.info(("clan activity:", clan_name))
        tstart = time.time()
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        clan_id = None
        if clan_name is None:
            player_name = await self.bot.wg.get_local_name(ctx)
            self.bot.logger.info(("clan activity:", clan_name))
            player, region = await self.bot.wg.get_player_a(player_name, ctx.message)
            self.bot.logger.info(("clan activity, player, region:", player, region))
            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]
                # clan=self.bot.wg.wotb_servers[region].clans.accountinfo(account_id=player_id)
                clan = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="clans",
                    cmd_path="accountinfo",
                    parameter="account_id={}".format(player_id),
                )
                if clan[player_id] is None:
                    await ctx.send(
                        "Player `{}@{}` has no active clan.".format(player_nick, region)
                    )
                    return
                if clan[player_id]["clan_id"] is None:
                    await ctx.send(
                        "Player `{}@{}` has no active clan.".format(player_nick, region)
                    )
                    return
                # clan=wotb.clans.accountinfo(account_id=player_id)
                clan_id = str(clan[player_id].get("clan_id", None))
                # process_clan(clan_id)
            else:
                out = await self.bot.wg.search_player_a(ctx, player_name)
                await self.bot.dc.not_found_msg(ctx, out)
        else:
            self.bot.logger.debug("got clan name {}".format(clan_name))
            # clan_list=wotb.clans.list(fields="clan_id,tag,name",search=clan_name)
            clan_list, region = await self.bot.wg.get_clan_a(clan_name)
            self.bot.logger.debug("got this from search: {}".format(clan_list))
            if clan_list:
                # clan_id=str(clan[player_id]['clan_id'])
                clan_id = str(clan_list[0]["clan_id"])
                # process_clan(clan_id)
            else:
                await ctx.send("clan not found")

        if clan_id is not None:
            # clan_info=self.bot.wg.wotb_servers[region].clans.info(clan_id=clan_id)
            clan_info = await self.bot.wg.get_wg(
                region=region,
                cmd_group="clans",
                cmd_path="info",
                parameter="clan_id={}".format(clan_id),
            )
            clan_tag = clan_info[clan_id]["tag"]
            clan_name = clan_info[clan_id]["name"]
            clan_emblem = clan_info[clan_id]["emblem_set_id"]
            clan_motto = clan_info[clan_id]["motto"]
            clan_members_count = clan_info[clan_id]["members_count"]
            clan_members_list_helper = clan_info[clan_id]["members_ids"]
            save_members_list = []
            members_names = {}

            embed = discord.Embed(
                title=clan_tag, description=clan_name, colour=234, type="rich"
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            embed.set_thumbnail(
                url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                )
            )
            # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(mark))
            embed.set_footer(
                text="{}".format(clan_motto),
                icon_url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                ),
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content="Please enable Embed links permission for wotbot."
                )

            # for i in clan_members_list_helper:
            # id, battles count,wins,rating
            # save_members_list.append([i,0,0,0])

            clan_players = ",".join(str(x) for x in clan_members_list_helper)
            # def a():
            #    return self.bot.wg.get_players_by_ids(clan_players,region)
            # future=self.bot.loop.run_in_executor(None,a)
            # clan_players_returned, region =await future
            clan_players_returned, region = await self.bot.wg.get_players_by_ids_a(
                clan_players, region
            )
            for clan_member, member_info in clan_players_returned.items():
                clan_member = int(clan_member)
                now = datetime.datetime.now()
                if member_info is not None:
                    self.bot.logger.debug(("process:", clan_member))
                    members_names[clan_member] = member_info["nickname"]
                    save_members_list.append(
                        [
                            clan_member,
                            member_info["statistics"]["all"]["battles"],
                            member_info["statistics"]["all"]["wins"],
                            member_info["statistics"]["all"]["damage_dealt"],
                        ]
                    )
                # clan_members_list=sorted(clan_members_list, key=itemgetter(3,5),reverse=True)
                # here we will subtract second list later

            my_file = Path(
                "./stats/{}-{}.pickle".format(ctx.message.author.id, clan_id)
            )
            self.bot.logger.debug(my_file)
            old_members_list = None
            if my_file.is_file():
                with open(
                    "./stats/{}-{}.pickle".format(ctx.message.author.id, clan_id), "rb"
                ) as storage:
                    old_members_list = pickle.load(storage)
            else:
                with open(
                    "./stats/{}-{}.pickle".format(ctx.message.author.id, clan_id), "wb"
                ) as storage:
                    pickle.dump([datetime.datetime.now(), save_members_list], storage)
                    await ctx.send(
                        "No data. Saving now. After some battles, try again ?clanstats"
                    )
                    return
            if old_members_list is not None:
                ago = datetime.datetime.now() - old_members_list[0]
                ago = humanize.naturaltime(ago)
                # self.bot.logger.debug(old_tiers[1])
                temp_members_list = []
                for i, u in enumerate(save_members_list):
                    for j, v in enumerate(old_members_list[1]):
                        if u[0] == v[0]:
                            if u[1] - v[1]:
                                temp_members_list.append(
                                    [u[0], u[1] - v[1], u[2] - v[2], u[3] - v[3]]
                                )

                clan_members_list = []
                total_battles = 0
                total_wins = 0
                total_dmg = 0
                total_wins_ = ""
                total_dmg_ = ""
                for x, v in enumerate(temp_members_list):
                    total_battles += v[1]
                    total_wins += v[2]
                    total_dmg += v[3]
                    try:
                        clan_members_list.append(
                            [v[0], v[1], (v[2] / v[1]) * 100, v[3] / v[1]]
                        )
                    except:
                        clan_members_list.append([v[0], v[1], 0, v[3]])
                if total_wins:
                    total_wins_ = (total_wins / total_battles) * 100
                if total_dmg:
                    total_dmg_ = total_dmg / total_battles

                # clan_members_list=sorted(clan_members_list, key=lambda x: (x[1],-x[2]))
                clan_members_list = sorted(
                    clan_members_list, key=lambda x: (-x[1], x[2])
                )

            out = "```md\n{}@{}, {} members:\n\n".format(
                clan_tag, region, clan_members_count
            )
            out += "Battle count and WinRate difference since {}.\nCreate new checkpoint at any time with ?clansavestats\n\n".format(
                ago
            )
            out += "{: <15} {: >7} {: >6} {: >7}\n```".format(
                "playername", "battles", "wr", "damage"
            )
            await ctx.send(out)
            await self.bot.dc.typing(ctx)

            # self.bot.logger.debug(clan_members_list)
            # self.bot.logger.debug(members_names)
            out_total = "```md\n"
            for i in clan_members_list:
                out_total += "{: <16} {: >6} {: >6.2f} {: >7.2f}\n".format(
                    members_names.get(i[0], "gone")[:15], i[1], i[2], i[3]
                )
            if total_wins:
                out_total += "{: <16} {: >6} {: >6.2f} {: >7.2f}\n".format(
                    "Total:", total_battles, total_wins_, total_dmg_
                )
            out_total += "```"
            self.bot.logger.debug(len(out_total))
            if len(clan_members_list) < 1:
                out_total = "```No data```"

            if len(out_total) > 2000:
                # just no time and energy to do better now
                out_list = "```md\n"
                for i in clan_members_list[: math.ceil(len(clan_members_list) / 2.0)]:
                    out_list += "{: <16} {: >6} {: >6.2f} {: >7.2f}\n".format(
                        members_names.get(i[0], "gone")[:15], i[1], i[2], i[3]
                    )
                out_list += "```"
                await ctx.send(out_list)
                out_list = "```md\n"
                for i in clan_members_list[
                    len(clan_members_list) - math.floor(len(clan_members_list) / 2.0) :
                ]:
                    out_list += "{: <16} {: >6} {: >6.2f} {: >7.2f}\n".format(
                        members_names.get(i[0], "gone")[:15], i[1], i[2], i[3]
                    )
                if total_wins:
                    out_list += "{: <16} {: >6} {: >6.2f} {: >7.2f}\n".format(
                        "Total:", total_battles, total_wins_, total_dmg_
                    )
                out_list += "```"
                await ctx.send(out_list)
            else:
                self.bot.logger.debug(len(out_total))
                await ctx.send(out_total)
            # await ctx.send(out_total)
            # self.bot.logger.debug(clan_members_list)
            print(humanize.naturaldelta(tstart - time.time()))
            self.bot.logger.info("end of clan stats task")


def setup(bot):
    bot.add_cog(ClanStats(bot))
