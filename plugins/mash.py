import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
import math
from tabulate import tabulate
import humanize
import time


class Utilities:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        hidden=True,
        aliases=[
            "skip",
            "play",
            "queue",
            "queu",
            "role",
            "modules",
            "?",
            "purge",
            "stats",
            "??",
            "wedgy",
            "purge30",
            "wedg",
            "clean",
            "clear",
            "volume",
            "mute",
            "unmute",
            "???",
            "????",
            "?????",
            "rps",
            "clearwarnings",
            "reason",
            "autopurge",
            "announce",
            "warn",
            "reprimand",
            "kill",
            "warnings",
            "nick",
            "ban",
            "afk",
            "stop",
            "pause",
            "say",
            "kick",
            "join",
            "playlist",
            "tag",
            "eval",
            "reminder",
            "count",
            "?join",
        ],
    )
    # @asyncio.coroutine
    async def command_doesn_exist(self, ctx):
        return

    @commands.command(hidden=True, pass_context=True)
    # @asyncio.coroutine
    @commands.is_owner()
    async def what(self, ctx):
        """Reply to owner with a m*a*s*h* quote."""
        # todo: add more from https://www.reddit.com/r/mash/comments/1a9cpl/what_mash_quotes_do_you_use_in_everyday_life/
        quote = "I have been busting the MM, to the benefit of all!"
        # quote="I'm pursuing my lifelong quest for the perfect, the absolutely driest martini to be found in this or any other world. And I think I may have hit upon the perfect formula. You pour six jiggers of gin, and you drink it while staring at a picture of Lorenzo Schwartz, the inventor of vermouth"
        embed = discord.Embed(
            title="Hi master!", description=quote, colour=1747484, type="rich"
        )
        embed.set_image(
            url="https://cdn.discordapp.com/attachments/414445407729352704/451753752001773578/mash-s4.m.jpg"
        )
        embed.set_footer(
            text="M*A*S*H",
            icon_url="https://user-images.githubusercontent.com/3680926/33114003-17a4786c-cf5b-11e7-996a-005ccce11733.jpg",
        )
        await ctx.send(content=None, embed=embed)

    @commands.command(hidden=True, pass_context=True)
    # @asyncio.coroutine
    @commands.is_owner()
    async def hi(self, ctx):
        """Reply to owner with a m*a*s*h* quote."""
        # todo: add more from https://www.reddit.com/r/mash/comments/1a9cpl/what_mash_quotes_do_you_use_in_everyday_life/
        quote = "It's nice to be nice to the nice"
        quote = "I'm pursuing my lifelong quest for the perfect, the absolutely driest martini to be found in this or any other world. And I think I may have hit upon the perfect formula. You pour six jiggers of gin, and you drink it while staring at a picture of Lorenzo Schwartz, the inventor of vermouth"
        embed = discord.Embed(
            title="Hi master!", description=quote, colour=1747484, type="rich"
        )
        embed.set_thumbnail(
            url="https://user-images.githubusercontent.com/3680926/33114681-425e963e-cf5e-11e7-822c-2511000c281c.jpg"
        )
        embed.set_footer(
            text="M*A*S*H S01E04",
            icon_url="https://user-images.githubusercontent.com/3680926/33114003-17a4786c-cf5b-11e7-996a-005ccce11733.jpg",
        )
        await ctx.send(content=None, embed=embed)

    @commands.command(hidden=True, pass_context=True)
    # @asyncio.coroutine
    @commands.is_owner()
    async def thank(self, ctx):
        """Reply to owner with a m*a*s*h* quote."""
        # todo: add more from https://www.reddit.com/r/mash/comments/1a9cpl/what_mash_quotes_do_you_use_in_everyday_life/
        quote = "It's nice to be nice to the nice"
        embed = discord.Embed(
            title="You seem to have superpower, master!",
            description=quote,
            colour=16750848,
            type="rich",
        )
        embed.set_thumbnail(
            url="https://user-images.githubusercontent.com/3680926/33114002-1784ffc8-cf5b-11e7-8e77-eb4b04f61074.jpg"
        )
        embed.set_footer(
            text="M*A*S*H S02E16",
            icon_url="https://user-images.githubusercontent.com/3680926/33114003-17a4786c-cf5b-11e7-996a-005ccce11733.jpg",
        )
        await ctx.send(content=None, embed=embed)

    @commands.command(hidden=True, pass_context=True)
    # @asyncio.coroutine
    @commands.is_owner()
    async def please(self, ctx):
        """Reply to owner with a m*a*s*h* quote."""
        # todo: add more from https://www.reddit.com/r/mash/comments/1a9cpl/what_mash_quotes_do_you_use_in_everyday_life/
        quote = "At your command!"
        embed = discord.Embed(
            title="OK master!", description=quote, colour=16750848, type="rich"
        )
        embed.set_thumbnail(
            url="https://user-images.githubusercontent.com/3680926/33114002-1784ffc8-cf5b-11e7-8e77-eb4b04f61074.jpg"
        )
        embed.set_footer(
            text="",
            icon_url="https://user-images.githubusercontent.com/3680926/33114003-17a4786c-cf5b-11e7-996a-005ccce11733.jpg",
        )
        await ctx.send(content=None, embed=embed)


def setup(bot):
    bot.add_cog(Utilities(bot))
