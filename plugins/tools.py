import discord
from discord.ext import commands

# import asyncio
import sys
import math


class Utilities:
    def __init__(self, bot):
        self.bot = bot

    @commands.is_owner()
    @commands.command(pass_context=True, hidden=True)
    async def show_playername(self, ctx, id: str = None):
        user = await self.bot.get_user_info(id)
        if id in self.bot.PlayerCfg:
            playername = self.bot.PlayerCfg[id].get("playername", None)
            if playername:
                await ctx.send(
                    content="Playername for {} has been set to {}".format(
                        user.name, playername
                    )
                )
            else:
                await ctx.send(content="Name not set for {}".format(user.name))
        else:
            await ctx.send(content="Name not set for {}".format(user.name))

    @commands.command(hidden=True, pass_context=True)
    @commands.is_owner()
    async def server_leave(self, ctx, server_id):
        srvr = self.bot.get_guild(int(server_id))
        await self.bot.leave_server(srvr)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def reload(self, ctx, ext_name: str):
        self.bot.logger.info("reload module {}".format(ext_name))
        try:
            self.bot.unload_extension("plugins.{}".format(ext_name))
        except:
            pass
        try:
            self.bot.load_extension("plugins.{}".format(ext_name))
        except Exception as e:
            exc = "{}: {}".format(type(e).__name__, e)
            self.bot.logger.error(
                ("Failed to load extension {}\n{}".format(ext_name, iexc))
            )
        return True

    @commands.command(hidden=True)
    @commands.is_owner()
    async def reload_all(self, ctx):
        extensions_list = self.bot.cfg["Plugins"]
        for ext_item in extensions_list:
            self.bot.logger.info("reload module {}".format(ext_item))
            try:
                self.bot.unload_extension("plugins.{}".format(ext_item))
            except:
                pass
            try:
                self.bot.load_extension("plugins.{}".format(ext_item))
            except Exception as e:
                exc = "{}: {}".format(type(e).__name__, e)
                self.bot.logger.error(
                    ("Failed to load extension {}\n{}".format(ext_item, iexc))
                )

    @commands.command(hidden=True)
    @commands.is_owner()
    async def reload_models(self, ctx):
        from imp import reload as imp_reload
        from utils import models

        models = imp_reload(models)
        self.bot.sqlite = models.DB(self.bot.logger, self.bot)
        try:
            models = imp_reload(models)
            self.bot.sqlite = models.DB(self.bot.logger, self.bot)
        except Exception as e:
            exc = "{}: {}".format(type(e).__name__, e)
            self.bot.logger.error(
                ("Failed to load extension {}\n{}".format(ext_name, iexc))
            )

    @commands.command(hidden=True)
    @commands.is_owner()
    async def reload_gamedata(self, ctx):
        from imp import reload as imp_reload
        from utils import gamedata

        try:
            gamedata = imp_reload(gamedata)
            self.bot.gamedata = gamedata.Data()
        except Exception as e:
            exc = "{}: {}".format(type(e).__name__, e)
            self.bot.logger.error(
                ("Failed to load extension {}\n{}".format(ext_name, iexc))
            )

    @commands.command(hidden=True)
    @commands.is_owner()
    async def reload_dc(self, ctx):
        from imp import reload as imp_reload
        from utils import dc_utils

        try:
            dc_utils = imp_reload(dc_utils)
            self.bot.dc = dc_utils.DC(self.bot)
        except Exception as e:
            exc = "{}: {}".format(type(e).__name__, e)
            self.bot.logger.error(
                ("Failed to load extension {}\n{}".format(ext_name, iexc))
            )

    @commands.command(hidden=True)
    @commands.is_owner()
    async def reload_wg(self, ctx):
        from imp import reload as imp_reload
        from utils import wg_utils

        try:
            wg_utils = imp_reload(wg_utils)
            self.bot.wg = wg_utils.WG(self.bot.cfg["WargamingToken"], self.bot)
        except Exception as e:
            exc = "{}: {}".format(type(e).__name__, e)
            self.bot.logger.error(
                ("Failed to load extension {}\n{}".format(ext_name, iexc))
            )

    @commands.command(hidden=True)
    @commands.is_owner()
    async def channel(self, ctx, channel_id: int):
        """msg id, channel id"""
        channel = self.bot.get_channel(channel_id)
        print(channel.name)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def delete(self, ctx, msg_id: int, channel_id: int):
        """msg id, channel id"""
        self.bot.logger.info("delete messege {}".format(msg_id))
        channel = self.bot.get_channel(channel_id)
        m = await channel.get_message(msg_id)
        if m:
            await m.delete()

    @commands.command(hidden=True)
    @commands.is_owner()
    async def load(self, ctx, ext_name: str):
        self.bot.logger.info(("load module:", ext_name))
        try:
            self.bot.load_extension("plugins.{}".format(ext_name))
        except Exception as e:
            exc = "{}: {}".format(type(e).__name__, e)
            self.bot.logger.error(
                "Failed to load extension {}\n{}".format(ext_name, iexc)
            )

    @commands.command(hidden=True)
    @commands.is_owner()
    async def unload(self, ctx, ext_name: str):
        self.bot.logger.info(("unload module:", ext_name))
        self.bot.unload_extension("plugins.{}".format(ext_name))

    @commands.command(hidden=True, pass_context=True)
    @commands.is_owner()
    async def list_servers(self, ctx):
        """List servers with wotbot."""
        out = []
        for server in self.bot.guilds:
            if "Discord Bots" in server.name:
                print(server.name, server.id, server.member_count)
            out.append([str(server.name), str(server.member_count)])
        # out.sort()
        # sorted(a, key=lambda tup: tup[1],reverse=True)
        out.sort(key=lambda tup: int(tup[1]), reverse=True)
        # print(out)
        lines_ = 80
        split_ = math.ceil(len(out) / lines_)
        # print(len(out),split_)

        start = 0
        end = lines_
        for i in range(0, split_):
            # print(i,start,end)
            # l='\n'.join(':'.join([x for x in out[start:end]]))
            l = "\n".join([":".join(y) for y in [x for x in out[start:end]]])
            await ctx.send("{}".format(l))
            start += lines_
            end += lines_

    @commands.command(hidden=True, pass_context=True)
    @commands.is_owner()
    async def list_channels(self, ctx):
        """List servers with wotbot."""
        for server in self.bot.guilds:
            # await self.bot.whisper("{} {}".format(server,server.member_count))
            self.bot.logger.debug(server)
            for channel in server.channels:
                if channel.permissions_for(server.me).send_messages:
                    self.bot.logger.info(("\t", channel))
                    pass

    @commands.command(hidden=True, pass_context=True)
    @commands.is_owner()
    async def list_online(self, ctx):
        """List servers with wotbot."""
        online = 0
        offline = 0
        idle = 0
        other = 0
        servers = 0
        for server_id in self.bot.ServerCfg:
            if "platoon-channel" in self.bot.ServerCfg[server_id]:
                server = self.bot.get_guild(int(server_id))
                if server is not None:
                    self.bot.logger.info(server.name)
                    servers += 1
                    for member in server.members:
                        if member.status == discord.Status.online:
                            online += 1
                        elif member.status == discord.Status.offline:
                            offline += 1
                        elif member.status == discord.Status.idle:
                            idle += 1
                        else:
                            other += 1
        self.bot.logger.info((servers, online, offline, idle, other))
        return (servers, online, offline, idle, other)

    @commands.is_owner()
    @commands.command(pass_context=True, hidden=True)
    async def memory(self, ctx):
        self.bot.logger.info("memory")
        import tracemalloc

        snapshot = tracemalloc.take_snapshot()
        top_stats = snapshot.statistics("lineno")

        print("[ Top 20 ]")
        for stat in top_stats[:20]:
            print(stat)

    @commands.is_owner()
    @commands.command(pass_context=True, hidden=True)
    async def top(self, ctx):
        self.bot.logger.info("top")
        import linecache
        import os
        import tracemalloc

        def display_top(snapshot, key_type="lineno", limit=20):
            snapshot = snapshot.filter_traces(
                (
                    tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
                    tracemalloc.Filter(False, "<unknown>"),
                )
            )
            top_stats = snapshot.statistics(key_type)

            print("Top %s lines" % limit)
            for index, stat in enumerate(top_stats[:limit], 1):
                frame = stat.traceback[0]
                # replace "/path/to/module/file.py" with "module/file.py"
                filename = os.sep.join(frame.filename.split(os.sep)[-2:])
                print(
                    "#%s: %s:%s: %.1f KiB"
                    % (index, filename, frame.lineno, stat.size / 1024)
                )
                line = linecache.getline(frame.filename, frame.lineno).strip()
                if line:
                    print("    %s" % line)

            other = top_stats[limit:]
            if other:
                size = sum(stat.size for stat in other)
                print("%s other: %.1f KiB" % (len(other), size / 1024))
            total = sum(stat.size for stat in top_stats)
            print("Total allocated size: %.1f KiB" % (total / 1024))

        snapshot = tracemalloc.take_snapshot()
        display_top(snapshot)

    @commands.is_owner()
    @commands.command(pass_context=True, hidden=True)
    # @asyncio.coroutine
    async def mil(self, ctx):
        self.bot.logger.info("million")
        # ch=self.bot.get_channel('418866225104420865')
        ch = discord.Object(id="418866225104420865")
        # ch=discord.Object(id='402196519991902218')
        # print(ctx.message.channel)
        async for msg in self.bot.logs_from(ch, limit=10):
            print(msg)
        # for i in msg:
        #    print(i)
        return None

    @commands.is_owner()
    @commands.command(pass_context=True, hidden=True)
    # @asyncio.coroutine
    async def is_bot(self, ctx, id):
        """Is this ID bot?"""
        user = await self.bot.get_user_info(id)
        print(user)
        print(user.bot)
        user = ctx.message.author
        print(user)
        print(user.bot)

    @commands.command(pass_context=True, hidden=False, aliases=["about"])
    # @asyncio.coroutine
    async def info(self, ctx):
        """info command"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        msg = _(
            "Made by b48g55m#3941\n\nStanding on the shoulders of giants:\n- linux\n- python\n- discord.py\n\nTo see source, help or report bugs: https://gitlab.com/vanous/wotbot.\n\nUse `?invite` to invite WotBot to your server.\n\nSee `?help` for help\nSee `?botstats` for nerdy statistics."
        )

        await ctx.send(msg)

    @commands.is_owner()
    @commands.command(parent="Utilities", hidden=True)
    async def do_exit(self, ctx):
        # sys.exit()
        pass

    @commands.is_owner()
    @commands.command(parent="Utilities", hidden=True)
    async def version(self):
        """print version"""
        v = await self.bot.sqlite.version()
        await ctx.send(content=v)


def setup(bot):
    bot.add_cog(Utilities(bot))
