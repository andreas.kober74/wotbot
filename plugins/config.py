import discord
from discord.ext import commands

# import asyncio
import re


def is_server_owner():
    async def predicate(ctx):
        if not ctx.message.author.permissions_in(ctx.message.channel).administrator:
            await ctx.send(content="Must be channel admin")
        return ctx.message.author.permissions_in(ctx.message.channel).administrator

    return commands.check(predicate)


class Config:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(
        pass_context=True, aliases=["?conf", "config", "configure", "prefix"]
    )
    async def conf(self, ctx):
        """Configure some settings for wotbot"""
        self.bot.logger.info("configure")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ctx.invoked_subcommand is None:
            await ctx.send(
                content=_(
                    "To edit preferences, you can also use WotBot Dashboard: https://wotbot.pythonanywhere.com/"
                )
            )
            await ctx.send(content=_("need more directions, type @wotbot help conf"))
            if ctx.message.guild is not None:
                if str(ctx.message.guild.id) in self.bot.ServerCfg:
                    print("pass")
                    for k in self.bot.ServerCfg[str(ctx.message.guild.id)]:
                        # print(k)
                        if any(
                            x in k
                            for x in [
                                "platoon-channel",
                                "updates-channel",
                                "replay-channel",
                                "replay-private",
                            ]
                        ):
                            channel_id = self.bot.ServerCfg[
                                str(ctx.message.guild.id)
                            ].get(k, None)
                            if channel_id is not None:
                                if type(channel_id) is list:
                                    ch_names = []
                                    for ch_id in channel_id:
                                        chan = self.bot.get_channel(int(ch_id))
                                        if chan is not None:
                                            ch_names.append(chan.name)
                                    await ctx.send(
                                        content=_("Server {}, set to: {}").format(
                                            k, ",".join(ch_names)
                                        )
                                    )
                                else:
                                    chan = self.bot.get_channel(int(channel_id))
                                    if chan is not None:
                                        await ctx.send(
                                            content=_("Server {}, set to: {}").format(
                                                k, chan.name
                                            )
                                        )
                        else:
                            await ctx.send(
                                content=_("Server {}, set to: {}").format(
                                    k, self.bot.ServerCfg[str(ctx.message.guild.id)][k]
                                )
                            )
                if str(ctx.message.author.id) in self.bot.PlayerCfg:
                    for s, t in self.bot.PlayerCfg[str(ctx.message.author.id)].items():
                        if "confirmedtoken" not in s:
                            if t != "":
                                await ctx.send(
                                    content=_("Your {}, set to: {}").format(s, t)
                                )
            else:
                await ctx.send(
                    content=_("Must run in a channel, not in Direct message")
                )

    @is_server_owner()
    @conf.command(pass_context=True, name="prefix")
    async def pref(self, ctx, _character: str = None):
        """Set your server prefix, one character, no space."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if _character is not None:
            if " " not in _character[:1]:
                if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                    self.bot.ServerCfg[str(ctx.message.guild.id)] = {}
                self.bot.ServerCfg[str(ctx.message.guild.id)]["prefix"] = _character[:1]
                self.bot.save_settings()
                await ctx.send(
                    content=_("New wotbot prefix is {}").format(_character[:1])
                )
            else:
                await ctx.send(content=_("Not a valid letter/character."))
        else:
            await ctx.send(content=_("Not a valid letter/character."))

    @is_server_owner()
    @conf.command(
        pass_context=True,
        name="replay-length",
        aliases=["replay-lenght", "replay-lentgh"],
    )
    async def replay_length(self, ctx, word: str = None):
        """Length of replay reply, one of: short medium long"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        correct = re.compile("(short|medium|long)$")  # allowed regions
        if word is not None:
            if correct.match(word):
                if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                    self.bot.ServerCfg[str(ctx.message.guild.id)] = {}
                self.bot.ServerCfg[str(ctx.message.guild.id)]["replay-length"] = word
                self.bot.save_settings()
                await ctx.send(
                    content=_(
                        "Replays reply length for this server is set to {}"
                    ).format(word)
                )
            else:
                await ctx.send(
                    content=_(
                        "Not a valid length, allowed is one of: short medium long"
                    )
                )
        else:
            await ctx.send(
                content=_("Not a valid length, allowed is one of: short medium long")
            )

    @is_server_owner()
    @conf.command(pass_context=True, name="language")
    async def lang(self, ctx, word: str = None):
        """Set server preferred language. Allowed is one of: en cs ru nl fr"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        correct = re.compile("(en|cs|ru|nl|fr)$")  # allowed regions
        if word is not None:
            if correct.match(word):
                if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                    self.bot.ServerCfg[str(ctx.message.guild.id)] = {}
                self.bot.ServerCfg[str(ctx.message.guild.id)]["language"] = word
                self.bot.save_settings()
                await ctx.send(
                    content=_("New language for this server is {}".format(word))
                )
            else:
                await ctx.send(content=_("Not a valid language"))
        else:
            await ctx.send(content=_("Not a valid language"))

    @is_server_owner()
    @conf.command(pass_context=True, name="region")
    async def region(self, ctx, word: str = None):
        """Set server preferred region. Allowed is one of: eu ru na asia"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        correct = re.compile("(eu|ru|na|asia)$")  # allowed regions
        if word is not None:
            if correct.match(word):
                if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                    self.bot.ServerCfg[str(ctx.message.guild.id)] = {}
                self.bot.ServerCfg[str(ctx.message.guild.id)]["region"] = word
                self.bot.save_settings()
                await ctx.send(
                    content=_("New region for this server is {}").format(word)
                )
            else:
                await ctx.send(content=_("Not a valid region"))
        else:
            await ctx.send(content=_("Not a valid region"))

    @is_server_owner()
    @conf.command(pass_context=True, name="replay-private")
    async def replay_private(self, ctx, action: str = None):
        """Set/Remove channel as _private_ bot can autoupload not publicly visible replays to WotInspector"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if action is not None:
            if action.lower() == "add":
                if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                    self.bot.ServerCfg[str(ctx.message.guild.id)] = {}
                if not self.bot.ServerCfg[str(ctx.message.guild.id)].get(
                    "replay-channel", None
                ):
                    self.bot.ServerCfg[str(ctx.message.guild.id)]["replay-channel"] = []
                if not self.bot.ServerCfg[str(ctx.message.guild.id)].get(
                    "replay-private", None
                ):
                    self.bot.ServerCfg[str(ctx.message.guild.id)]["replay-private"] = []
                if (
                    str(ctx.message.channel.id)
                    not in self.bot.ServerCfg[str(ctx.message.guild.id)][
                        "replay-channel"
                    ]
                ):
                    self.bot.ServerCfg[str(ctx.message.guild.id)][
                        "replay-channel"
                    ].append(str(ctx.message.channel.id))
                    await ctx.send(
                        content=_(
                            "{} was previously not set as replay-channel, it was now added automatically."
                        ).format(ctx.message.channel.name)
                    )
                if (
                    str(ctx.message.channel.id)
                    not in self.bot.ServerCfg[str(ctx.message.guild.id)][
                        "replay-private"
                    ]
                ):
                    self.bot.ServerCfg[str(ctx.message.guild.id)][
                        "replay-private"
                    ].append(str(ctx.message.channel.id))
                self.bot.save_settings()
                await ctx.send(
                    content=_("{} was set as replay-private").format(
                        ctx.message.channel.name
                    )
                )
            elif action.lower() == "remove":
                if str(ctx.message.guild.id) in self.bot.ServerCfg:
                    if (
                        "replay-private"
                        in self.bot.ServerCfg[str(ctx.message.guild.id)]
                    ):
                        if (
                            str(ctx.message.channel.id)
                            in self.bot.ServerCfg[str(ctx.message.guild.id)][
                                "replay-private"
                            ]
                        ):
                            self.bot.ServerCfg[str(ctx.message.guild.id)][
                                "replay-private"
                            ].remove(str(ctx.message.channel.id))
                            self.bot.save_settings()
                            await ctx.send(
                                content=_("{} removed from replay-private").format(
                                    ctx.message.channel.name
                                )
                            )
            else:
                await ctx.send(content=_("Not a valid option. Use add or remove."))
        else:
            await ctx.send(content=_("Not a valid option. Use add or remove."))

    @is_server_owner()
    @conf.command(pass_context=True, name="replay-channel")
    async def replay_channel(self, ctx, action: str = None):
        """Add/Remove channel where bot can autoupload replays to WotInspector"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if action is not None:
            if action.lower() == "add":
                if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                    self.bot.ServerCfg[str(ctx.message.guild.id)] = {}
                if not self.bot.ServerCfg[str(ctx.message.guild.id)].get(
                    "replay-channel", None
                ):
                    self.bot.ServerCfg[str(ctx.message.guild.id)]["replay-channel"] = []
                if (
                    str(ctx.message.channel.id)
                    not in self.bot.ServerCfg[str(ctx.message.guild.id)][
                        "replay-channel"
                    ]
                ):
                    self.bot.ServerCfg[str(ctx.message.guild.id)][
                        "replay-channel"
                    ].append(str(ctx.message.channel.id))
                self.bot.save_settings()
                await ctx.send(
                    content=_("{} was added as replay-channel for this server.").format(
                        ctx.message.channel.name
                    )
                )
            elif action.lower() == "remove":
                if str(ctx.message.guild.id) in self.bot.ServerCfg:
                    if (
                        "replay-channel"
                        in self.bot.ServerCfg[str(ctx.message.guild.id)]
                    ):
                        if (
                            str(ctx.message.channel.id)
                            in self.bot.ServerCfg[str(ctx.message.guild.id)][
                                "replay-channel"
                            ]
                        ):
                            self.bot.ServerCfg[str(ctx.message.guild.id)][
                                "replay-channel"
                            ].remove(str(ctx.message.channel.id))
                            self.bot.save_settings()
                            await ctx.send(
                                content=_("{} removed from replay-channel").format(
                                    ctx.message.channel.name
                                )
                            )
            else:
                await ctx.send(content=_("Not a valid option. Use add or remove."))
        else:
            await ctx.send(content=_("Not a valid option. Use add or remove."))

    @is_server_owner()
    @conf.command(pass_context=True, name="platoon-channel")
    async def platoon_channel(self, ctx, action: str = None):
        """Add/Remove chanel where to receive platoon notifications"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if action is not None:
            if action.lower() == "add":
                if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                    self.bot.ServerCfg[str(ctx.message.guild.id)] = {}
                self.bot.ServerCfg[str(ctx.message.guild.id)]["platoon-channel"] = str(
                    ctx.message.channel.id
                )
                self.bot.save_settings()
                await ctx.send(
                    content=_("New platoon-channel for this server is {}").format(
                        ctx.message.channel.name
                    )
                )
            elif action.lower() == "remove":
                if str(ctx.message.guild.id) in self.bot.ServerCfg:
                    if (
                        "platoon-channel"
                        in self.bot.ServerCfg[str(ctx.message.guild.id)]
                    ):
                        self.bot.ServerCfg[str(ctx.message.guild.id)].pop(
                            "platoon-channel"
                        )
                        self.bot.save_settings()
                        await ctx.send(content=_("Platoon-channel removed"))
            else:
                await ctx.send(content=_("Not a valid option. Use add or remove."))
        else:
            await ctx.send(content=_("Not a valid option. Use add or remove."))

    @is_server_owner()
    @conf.command(pass_context=True, name="updates-channel")
    async def updates_channel(self, ctx, action: str = None):
        """Add/Remove channel where to receive notifications about new wotbot features"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if action is not None:
            if action.lower() == "add":
                if str(ctx.message.guild.id) not in self.bot.ServerCfg:
                    self.bot.ServerCfg[str(ctx.message.guild.id)] = {}
                self.bot.ServerCfg[str(ctx.message.guild.id)]["updates-channel"] = str(
                    ctx.message.channel.id
                )
                self.bot.save_settings()
                await ctx.send(
                    content=_("New updates-channel for this server is {}").format(
                        ctx.message.channel.name
                    )
                )
            elif action.lower() == "remove":
                if str(ctx.message.guild.id) in self.bot.ServerCfg:
                    if (
                        "updates-channel"
                        in self.bot.ServerCfg[str(ctx.message.guild.id)]
                    ):
                        self.bot.ServerCfg[str(ctx.message.guild.id)].pop(
                            "updates-channel"
                        )
                        self.bot.save_settings()
                        await ctx.send(content=_("Updates-channel removed"))
            else:
                await ctx.send(content=_("Not a valid option. Use add or remove."))
        else:
            await ctx.send(content=_("Not a valid option. Use add or remove."))

    @conf.command(pass_context=True, name="playerlang")
    async def playerlanguage(self, ctx, word: str = None):
        """Set your preferred language. One of: en cs fr nl ru"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        correct = re.compile("(en|cs|fr|ru|nl)$")  # allowed regions
        if word is not None:
            if correct.match(word):
                if str(ctx.message.author.id) not in self.bot.PlayerCfg:
                    self.bot.PlayerCfg[str(ctx.message.author.id)] = {}
                self.bot.PlayerCfg[str(ctx.message.author.id)]["language"] = word
                self.bot.save_player_settings()
                await ctx.send(content=_("Your preferred language is {}").format(word))
            else:
                await ctx.send(content=_("Not a valid language"))
        else:
            if str(ctx.message.author.id) in self.bot.PlayerCfg:
                playerlang = self.bot.PlayerCfg[str(ctx.message.author.id)].get(
                    "language", None
                )
                if playerlang is not None:
                    await ctx.send(
                        content=_("Your preferred language is {}").format(playerlang)
                    )
            else:
                await ctx.send(content=_("usage: ?conf playerlang language"))

    @conf.command(pass_context=True, name="playername")
    async def playername(self, ctx, playername: str = None):
        """Set WG playername@region . Region is optional."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if playername is not None:
            if len(playername) > 3:
                if str(ctx.message.author.id) not in self.bot.PlayerCfg:
                    self.bot.PlayerCfg[str(ctx.message.author.id)] = {}

                self.bot.PlayerCfg[str(ctx.message.author.id)][
                    "playername"
                ] = playername
                self.bot.save_player_settings()

                # command = self.bot.all_commands["conf"]
                command = self.bot.get_command("conf playername")
                await ctx.invoke(command)
            else:
                await ctx.send(content=_("Not a valid playername"))
        else:
            if str(ctx.message.author.id) in self.bot.PlayerCfg:
                playername = self.bot.PlayerCfg[str(ctx.message.author.id)].get(
                    "confirmedname", None
                )
                if playername:
                    playerregion = self.bot.PlayerCfg[str(ctx.message.author.id)].get(
                        "confirmedregion", None
                    )
                    await ctx.send(
                        content=_(
                            ":ballot_box_with_check: Your confirmed and preferably used WG playername is {name}@{region}"
                        ).format(name=playername, region=playerregion)
                    )
                playername = self.bot.PlayerCfg[str(ctx.message.author.id)].get(
                    "playername", None
                )
                if playername is not None:
                    await ctx.send(
                        content=_("Your set WG playername is {}").format(playername)
                    )
            else:
                await ctx.send(
                    content=_(
                        "Use WotBot Dashboard: https://wotbot.pythonanywhere.com/ to set confirmed WG name. \nIf you use: ?conf playername <playername> , if will have less features."
                    )
                )


def setup(bot):
    bot.add_cog(Config(bot))
