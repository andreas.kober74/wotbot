import discord
from discord.ext import commands
from discord.ext.commands import bot as bot_module
import asyncio

from discord.ext.commands.formatter import HelpFormatter


class Utilities:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, name="webhelp", hidden=True)
    # @asyncio.coroutine
    async def web_help(self, ctx):
        """Shows this message."""
        self.bot.logger.info("webhelp")

        help_data = await self.bot.webhelp.format(ctx, self.bot)
        with open(self.bot.cfg["WebHelpDataFile"], "w") as data_js:
            data_js.write(help_data)


def setup(bot):
    bot.add_cog(Utilities(bot))
